package bolelecke.java.ecgheler.core;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import boelecke.java.audiogen.audiogen;
import boelecke.java.gdt.*;



public class Core 
{
	private audiogen generator;
	private GDTReader reader;
	private GDTWriter writer;
	private double restPulse;
	private double weight;
	private double age;
	private String name;
	private double start 	= 50; 			// in W
	private double g 		= 9.81;			// in m/s�
	private double h 		= 0.24; 				// in cm
	private int ns 			= 4;
	private double fitness 	= 1.7024;
	private PropertyChangeSupport propChangeSupport;
	private Clip clip;
	private int pos;
	private UpdatePos updateTask;
	private Timer timer;
	private float[] fstep;
	
	public Core() 
	{
		generator = new audiogen();
		writer = new GDTWriter();
		
		 propChangeSupport = new PropertyChangeSupport(this);
		
		updateTask = new UpdatePos();
		timer  = new Timer();
		
	}
	
	public boolean createGDTDir()
	{
		File dir = new File("C:\\GDT\\");
		return dir.mkdir();
	}
	
	public float[] calculateSteps()
	{
		double fpmax = (220-age);
		double ftmax = (fpmax-restPulse)/(fitness);
		
		
		
		double ftmin = start*60/(weight*g*h);
		
		
		float[] fstep = new float[ns];
		
		for (int i = 0; i < ns; i++)
		{
			fstep[i] = (float) (((ftmax-ftmin)/(ns-1))*i + ftmin);
		}
		
		return fstep;
	}
		
	public void generateMetronome()
	{
		
		float[] fstep2 = calculateSteps();
		propChangeSupport.firePropertyChange("fstep", fstep, fstep = fstep2 );
		generator.generateAudioWin(name, fstep);
	}

	public double getRestPulse() {
		return restPulse;
	}

	public void setRestPulse(double restPulse) 
	{
		this.restPulse = restPulse;
	}

	public double getWeight() 
	{
		return weight;
	}

	public void setWeight(double weight) 
	{
		this.weight = weight;
	}

	public double getAge() 
	{
		return age;
	}

	public void setAge(double age) 
	{
		this.age = age;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public void playMusic() throws UnsupportedAudioFileException, IOException, LineUnavailableException
	{
		File file = new File(name+".wav");
		
		if(clip == null )
		{
	
			AudioInputStream audioIn = AudioSystem.getAudioInputStream(file);
			clip = AudioSystem.getClip();
		
			clip.open(audioIn);
			clip.start();
			timer.schedule(updateTask, 0, 1000);    // time in ms
		}
		else
		{		
			if(clip.isRunning())
			{
				clip.stop();
			}
			else
			{
				clip.start();
			}
		}
	}

	public void stopMusic()
	{
		if (clip != null)
		{
			clip.stop();
			clip.close();
			updateTask.cancel();
		}
	}
	
	public int getPos()
	{
		if(clip != null)
		{
			int debuga = clip.getFramePosition();
			int debugb = clip.getFrameLength();
			return clip.getFramePosition()*100/clip.getFrameLength();
		}
		else
		{
			return 0;
		}
	}
	
	 public void addPropertyChangeListener(String propertyName,	 PropertyChangeListener listener) 
	 {
		propChangeSupport.addPropertyChangeListener(propertyName, listener);
	 } 
	
	
	/*-----------------------------------------------------------------Threads--------------------------------------------------------------------*/
	 class  UpdatePos extends TimerTask 
	 {
		@Override
		public void run() 
		{	
			int pos1 = getPos();
			
			propChangeSupport.firePropertyChange("pos", pos, pos =pos1 );
		}
	};
	

}
