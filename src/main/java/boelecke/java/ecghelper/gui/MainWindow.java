package boelecke.java.ecghelper.gui;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.ProgressBar;
import bolelecke.java.ecgheler.core.Core;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;

public class MainWindow {

	protected Shell shlEcgHelper;
	private Text text;
	private Text text_1;
	private Text text_2;
	private Text text_3;
	private Table table;
	private Button btnLaden;
	private Button btnPlaypause;
	private Button btnStop;
	private static Core core;
	private Display display;
	
	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) 
	{
		core = new Core();
		try 
		{
			
			MainWindow window = new MainWindow();
			window.open();
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() 
	{
		display = Display.getDefault();
		createContents();
		shlEcgHelper.open();
		shlEcgHelper.layout();
		while (!shlEcgHelper.isDisposed()) 
		{
			if (!display.readAndDispatch()) 
			{
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlEcgHelper = new Shell();
		shlEcgHelper.setSize(1034, 600);
		shlEcgHelper.setText("ECG Helper");
		shlEcgHelper.setLayout(new FormLayout());
		
		Group grpPatient = new Group(shlEcgHelper, SWT.NONE);
		FormData fd_grpPatient = new FormData();
		fd_grpPatient.right = new FormAttachment(100, -398);
		fd_grpPatient.bottom = new FormAttachment(100, -265);
		fd_grpPatient.top = new FormAttachment(100, -510);
		fd_grpPatient.left = new FormAttachment(100, -978);
		grpPatient.setLayoutData(fd_grpPatient);
		grpPatient.setText("Patient");
		
		text = new Text(grpPatient, SWT.BORDER);
		
		text.setBounds(196, 54, 243, 31);
		
		Label lblName = new Label(grpPatient, SWT.NONE);
		lblName.setBounds(58, 54, 81, 25);
		lblName.setText("Name");
		
		text_1 = new Text(grpPatient, SWT.BORDER);
		text_1.setBounds(196, 91, 243, 31);
		
		text_2 = new Text(grpPatient, SWT.BORDER);
		text_2.setBounds(196, 128, 243, 31);
		
		text_3 = new Text(grpPatient, SWT.BORDER);
		text_3.setBounds(196, 165, 243, 31);
		
		Label lblAlter = new Label(grpPatient, SWT.NONE);
		lblAlter.setText("Alter");
		lblAlter.setBounds(58, 91, 81, 25);
		
		Label lblGewicht = new Label(grpPatient, SWT.NONE);
		lblGewicht.setText("Gewicht");
		lblGewicht.setBounds(58, 128, 81, 25);
		
		Label lblRuhepuls = new Label(grpPatient, SWT.NONE);
		lblRuhepuls.setText("Ruhepuls");
		lblRuhepuls.setBounds(58, 165, 81, 25);
		
		table = new Table(shlEcgHelper, SWT.BORDER | SWT.FULL_SELECTION);
		FormData fd_table = new FormData();
		fd_table.right = new FormAttachment(100, -38);
		fd_table.left = new FormAttachment(grpPatient, 6);
		fd_table.bottom = new FormAttachment(100, -265);
		fd_table.top = new FormAttachment(0, 46);
		table.setLayoutData(fd_table);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		
		
		TableColumn[] column = new TableColumn[2];
		
		column[0] = new TableColumn(table, SWT.NONE);
		column[0].setText("Stufe");
		column[1] = new TableColumn(table, SWT.NONE);
		column[1].setText("f[1/min]");
		column[0].pack();
		column[1].pack();
		
		btnLaden = new Button(shlEcgHelper, SWT.NONE);
		btnLaden.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent arg0) 
			{
				
				core.setName(text.getText());
				core.setAge(Double.parseDouble(text_1.getText()));
				core.setRestPulse(Double.parseDouble(text_3.getText()));
				core.setWeight(Double.parseDouble(text_2.getText()));
						
				
				core.generateMetronome();
			}
		});
	
			
		btnLaden.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
			}
		});
		FormData fd_btnLaden = new FormData();
		fd_btnLaden.left = new FormAttachment(0, 34);
		fd_btnLaden.top = new FormAttachment(grpPatient, 35);
		btnLaden.setLayoutData(fd_btnLaden);
		btnLaden.setText("Berechnen");
		
		btnPlaypause = new Button(shlEcgHelper, SWT.NONE);
		btnPlaypause.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseDown(MouseEvent arg0) 
			{
				try 
				{
					core.playMusic();
				} 
				catch (UnsupportedAudioFileException e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				catch (IOException e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				catch (LineUnavailableException e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		fd_btnLaden.right = new FormAttachment(100, -879);
		btnPlaypause.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
			}
		});
		btnPlaypause.setText("Play/Pause");
		FormData fd_btnPlaypause = new FormData();
		fd_btnPlaypause.right = new FormAttachment(100, -774);
		fd_btnPlaypause.left = new FormAttachment(btnLaden, 6);
		fd_btnPlaypause.top = new FormAttachment(btnLaden, 0, SWT.TOP);
		btnPlaypause.setLayoutData(fd_btnPlaypause);
		
		btnStop = new Button(shlEcgHelper, SWT.NONE);
		btnStop.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent arg0) 
			{
				core.stopMusic();
			}
		});
		btnStop.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
			}
		});
		btnStop.setText("Stop");
		FormData fd_btnStop = new FormData();
		fd_btnStop.top = new FormAttachment(btnLaden, 0, SWT.TOP);
		fd_btnStop.right = new FormAttachment(grpPatient, -311, SWT.RIGHT);
		fd_btnStop.left = new FormAttachment(btnPlaypause, 6);
		btnStop.setLayoutData(fd_btnStop);
		
		final ProgressBar progressBar = new ProgressBar(shlEcgHelper, SWT.NONE);
		FormData fd_progressBar = new FormData();
		fd_progressBar.top = new FormAttachment(btnLaden, 6);
		fd_progressBar.right = new FormAttachment(btnStop, 0, SWT.RIGHT);
		fd_progressBar.left = new FormAttachment(grpPatient, 0, SWT.LEFT);
		fd_progressBar.bottom = new FormAttachment(100, -163);
		progressBar.setLayoutData(fd_progressBar);
		
		

	    
	   core.addPropertyChangeListener("pos", 
			   new PropertyChangeListener() 
	   			{
		   			public void propertyChange(final PropertyChangeEvent evt) 
		   			{	 
		   				display.asyncExec
		   				(
		   						new Runnable() 
		   						{
									
									public void run() 
									{
										progressBar.setSelection((Integer) evt.getNewValue());
										
									}
								}
   						);
		   				
		   				
		   			}
	   			}
	   );

		core.addPropertyChangeListener("fstep", 
				new PropertyChangeListener() 
				{
			
					public void propertyChange(PropertyChangeEvent evt) 
					{		float[] fstep = (float[]) evt.getNewValue();
							if(table.getItems().length == 0)
							{
								
								for (int i= 0; i< fstep.length; i++)
								{
									TableItem item = new TableItem(table, SWT.NONE);
									item.setText(0,String.valueOf(i));
									item.setText(1, String.valueOf(fstep[i]));
								}
							}
							else
							{
								
								for (int i= 0; i< fstep.length; i++)
								{
									TableItem item = table.getItems()[i];
									//item.setText(0,String.valueOf(i));
									item.setText(1, String.valueOf(fstep[i]));
								}
							}
							
							
					}
				}
		);

	}
}
