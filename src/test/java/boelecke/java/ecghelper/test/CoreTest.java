package boelecke.java.ecghelper.test;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Before;
import org.junit.Test;

import com.sun.xml.internal.messaging.saaj.util.ByteInputStream;

import boelecke.java.audiogen.audiogen;
import bolelecke.java.ecgheler.core.Core;

public class CoreTest 
{
	Core core;
	audiogen generator;
	float[] fstepResult;
	@Before
	public void setUp() throws Exception 
	{
		core = new Core();
		core.setAge(22);
		core.setName("TestCase");
		core.setRestPulse(80);
		core.setWeight(81);
		
		fstepResult =new float[]{15.9276f, 33.5272f, 51.1268f, 68.7264f };
		generator = new audiogen();
	}

	@Test
	public void testCalculateSteps() 
	{
		
	 assertArrayEquals(fstepResult, core.calculateSteps(),0.6f);
	}

	@Test
	public void testGenerateMetronome() throws IOException 
	{
		generator.generateAudioWin("C:\\Users\\admin\\Desktop\\TestCore1", fstepResult);
		generator.generateAudioWin("C:\\Users\\admin\\Desktop\\TestCore2", core.calculateSteps());
		
		File testFile1 = new File("C:\\Users\\admin\\Desktop\\TestCore1.wav");
		File testFile2 = new File("C:\\Users\\admin\\Desktop\\TestCore2.wav");
		try 
		{
			Thread.sleep(5000);
		} 
		catch (InterruptedException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue(testFile1.isFile());
		assertTrue(testFile1.exists());
		assertTrue(testFile2.exists());
		ByteArrayInputStream bIs1 = null;
		ByteArrayInputStream bIs2 = null;
		try 
		{
			byte[] buf1 = new byte[10];
			byte[] buf2 = new byte[10];
			bIs1 = new ByteArrayInputStream(buf1);
			bIs2 = new ByteArrayInputStream(buf2);
			
			while((-1 != bIs1.read(buf1)) && (-1 != bIs2.read(buf2))) 
			{
				assertArrayEquals(buf1, buf2);
				
			}
			
			
		} 
		catch (FileNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			bIs1.close();
			bIs2.close();
		}
		
		
	}

	
	
	
	@Test
	public void testPlayMusic()
	{
		try {
			core.playMusic();
		} catch (UnsupportedAudioFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
